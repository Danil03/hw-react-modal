import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './components/scss/main.scss';
import axios from 'axios';
import ProductList from './components/Cards/ProductList.jsx';
import Cart from './components/Cart/Cart.jsx';
import Favorites from './components/Favorites/Favorites.jsx';
import Header from './components/Header/Header.jsx';
import DisplayModeToggle from './components/Button/DisplayModeToggle.jsx';
import { DisplayModeProvider } from './components/context/DisplayModeContext.jsx';
import './App.css';

function App() {
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState([]);
  const [favorites, setFavorites] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('/products.json');
        setProducts(response.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    const savedFavorites = JSON.parse(localStorage.getItem('favorites')) || [];
    setFavorites(savedFavorites);
  }, []);

  useEffect(() => {
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }, [favorites]);

  const addToCart = (product) => {
    setCart((prevCart) => [...prevCart, product]);
  };

  const removeFromCart = (product) => {
    setCart((prevCart) => prevCart.filter((item) => item.sku !== product.sku));
  };

  const addToFavorites = (product) => {
    setFavorites((prevFavorites) => {
      if (!prevFavorites.some((item) => item.sku === product.sku)) {
        return [...prevFavorites, product];
      }
      return prevFavorites;
    });
  };

  const removeFromFavorites = (product) => {
    setFavorites((prevFavorites) => prevFavorites.filter((item) => item.sku !== product.sku));
  };

  return (
    <Router>
      <DisplayModeProvider>
        <div className="App">
          <Header cartItemsCount={cart.length} favoriteItemsCount={favorites.length} />
          <DisplayModeToggle />
          <Routes>
            <Route path="/" element={<ProductList products={products} addToCart={addToCart} addToFavorites={addToFavorites} removeFromFavorites={removeFromFavorites} />} />
            <Route path="/cart" element={<Cart cart={cart} removeFromCart={removeFromCart} />} />
            <Route path="/favorites" element={<Favorites favorites={favorites} removeFromFavorites={removeFromFavorites} />} />
          </Routes>
        </div>
      </DisplayModeProvider>
    </Router>
  );
}

export default App;