import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Cart from '../components/Cart/Cart';

test('removes item from cart', () => {
  const cart = [{ sku: '123', name: 'Test Product', price: 10, color: 'red', image: '' }];
  const removeFromCart = jest.fn();
  render(<Cart cart={cart} removeFromCart={removeFromCart} />);

  fireEvent.click(screen.getByText('Remove'));
  expect(screen.getByText('Are you sure you want to remove this item from the cart?')).toBeInTheDocument();

  fireEvent.click(screen.getByText('Confirm'));
  expect(removeFromCart).toHaveBeenCalledWith(cart[0]);
});