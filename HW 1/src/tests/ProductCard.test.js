import React from 'react';
import renderer from 'react-test-renderer';
import ProductCard from '../components/Cards/ProductCard';

test('renders correctly', () => {
  const product = { sku: '123', name: 'Test Product', price: 10, color: 'red', image: '' };
  const tree = renderer.create(
    <ProductCard product={product} addToCart={() => {}} />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});