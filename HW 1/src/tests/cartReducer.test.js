import { cartReducer } from '../reducers/cartReducer';

test('should handle ADD_TO_CART', () => {
  const initialState = { cart: [] };
  const action = { type: 'ADD_TO_CART', payload: { sku: '123', name: 'Test Product' } };
  const newState = cartReducer(initialState, action);
  expect(newState.cart).toHaveLength(1);
  expect(newState.cart[0]).toEqual(action.payload);
});

test('should handle REMOVE_FROM_CART', () => {
  const initialState = { cart: [{ sku: '123', name: 'Test Product' }] };
  const action = { type: 'REMOVE_FROM_CART', payload: { sku: '123' } };
  const newState = cartReducer(initialState, action);
  expect(newState.cart).toHaveLength(0);
});