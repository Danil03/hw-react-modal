import React from 'react';
import { Link } from 'react-router-dom';

function Header({ cartItemsCount, favoriteItemsCount }) {
  return (
    <div className="header">
      <div className="favorite-icon">
        <Link  to="/favorites">
          <span className="icon" aria-label="Favorite">&#9733;</span>
          <span className="item-count">{favoriteItemsCount}</span>
        </Link>
      </div>
      <div>
      <Link to="/" className="home">Home</Link>
      </div>
      <div className="cart-icon">
        <Link to="/cart">
          <span className="icon" aria-label="Cart">&#128722;</span>
          <span className="item-count">{cartItemsCount}</span>
        </Link>
      </div>
    </div>
  );
}

export default Header;