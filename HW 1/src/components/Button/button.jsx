// import './src/scss/button.scss';
function Button(props) {
   const {
    type, 
    classNames, 
    onClick, 
    children
    } = props

    return (
      <button className= {classNames} 
      type={type}
      onClick={onClick}
      >
       {children}
      </button>
    )
  }
  
  export default Button
  