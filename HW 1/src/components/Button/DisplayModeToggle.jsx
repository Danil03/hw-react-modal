import React from 'react';
import { useDisplayMode } from '../context/DisplayModeContext';
// import './DisplayModeToggle.scss';

function DisplayModeToggle() {
  const { isGridView, setIsGridView } = useDisplayMode();

  return (
    <div className="display-mode-toggle">
      <button onClick={() => setIsGridView(true)} className={isGridView ? 'active' : ''}>
        Grid View
      </button>
      <button onClick={() => setIsGridView(false)} className={!isGridView ? 'active' : ''}>
        Table View
      </button>
    </div>
  );
}

export default DisplayModeToggle;