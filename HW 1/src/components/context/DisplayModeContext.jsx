import React, { createContext, useContext, useState } from 'react';

const DisplayModeContext = createContext();

export const DisplayModeProvider = ({ children }) => {
  const [isGridView, setIsGridView] = useState(true);

  return (
    <DisplayModeContext.Provider value={{ isGridView, setIsGridView }}>
      {children}
    </DisplayModeContext.Provider>
  );
};

export const useDisplayMode = () => useContext(DisplayModeContext);