import React, { useState } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import ProductCard from '../Cards/ProductCard';
import ModalConfirm from '../modal/ModalConfirm';
// import './cart.scss';

function Cart({ cart, removeFromCart }) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);

  const handleRemoveClick = (product) => {
    setSelectedProduct(product);
    setIsModalOpen(true);
  };

  const confirmRemove = () => {
    removeFromCart(selectedProduct);
    setIsModalOpen(false);
  };

  const initialValues = {
    firstName: '',
    lastName: '',
    age: '',
    address: '',
    phone: '',
  };

  const validationSchema = Yup.object({
    firstName: Yup.string().required('Required'),
    lastName: Yup.string().required('Required'),
    age: Yup.number().required('Required').positive('Invalid age').integer('Invalid age'),
    address: Yup.string().required('Required'),
    phone: Yup.string().required('Required').matches(/^\d{10}$/, 'Invalid phone number'),
  });

  const handleSubmit = (values, { resetForm }) => {
    console.log('Purchased Items:', cart);
    console.log('User Information:', values);
    resetForm();
    localStorage.removeItem('cart');
  };

  return (
    <div className="cart-container">
      <div className="cart">
        <h2 className="shop-title">Shopping Cart</h2>
        {cart.map((product, index) => (
          <div key={index} className="cart-item">
            <ProductCard product={product} addToCart={() => {}} />
            <button className="remove-button" onClick={() => handleRemoveClick(product)}>Remove</button>
          </div>
        ))}
      </div>

      <div className="checkout-form-container">
        <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleSubmit}>
          {({ touched, errors }) => (
            <Form className="checkout-form">
              <h2 className="form-title">Checkout Information</h2>
              <div className="form-field">
                <label htmlFor="firstName">First Name</label>
                <Field name="firstName" type="text" />
                <ErrorMessage name="firstName" component="div" className="error" />
              </div>
              <div className="form-field">
                <label htmlFor="lastName">Last Name</label>
                <Field name="lastName" type="text" />
                <ErrorMessage name="lastName" component="div" className="error" />
              </div>
              <div className="form-field">
                <label htmlFor="age">Age</label>
                <Field name="age" type="number" />
                <ErrorMessage name="age" component="div" className="error" />
              </div>
              <div className="form-field">
                <label htmlFor="address">Address</label>
                <Field name="address" type="text" />
                <ErrorMessage name="address" component="div" className="error" />
              </div>
              <div className="form-field">
                <label htmlFor="phone">Phone</label>
                <Field name="phone" type="text" />
                <ErrorMessage name="phone" component="div" className="error" />
              </div>
              <button type="submit" className="checkout-button">Checkout</button>
            </Form>
          )}
        </Formik>
      </div>

      {isModalOpen && (
        <ModalConfirm onClose={() => setIsModalOpen(false)} onConfirm={confirmRemove}>
          Are you sure you want to remove this item from the cart?
        </ModalConfirm>
      )}
    </div>
  );
}

export default Cart;