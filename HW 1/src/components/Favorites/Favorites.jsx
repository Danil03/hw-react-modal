import React from 'react';
import ProductCard from '../Cards/ProductCard';

function Favorites({ favorites, removeFromFavorites }) {
  return (
    <div className="favorites">
      <h2 className='shop-title'>Favorites</h2>
      {favorites.map((product, index) => (
        <ProductCard 
          key={index} 
          product={product} 
          addToCart={() => {}} 
          addToFavorites={() => {}} 
          removeFromFavorites={removeFromFavorites} 
        />
      ))}
    </div>
  );
}

export default Favorites;