import React, { useState } from 'react';
import ModalText from '../modal/ModalText';

function ProductCard({ product, addToCart, addToFavorites, removeFromFavorites }) {
  const [isFavorite, setIsFavorite] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleAddToCart = () => {
    addToCart(product);
    setIsModalOpen(true);
  };

  const handleToggleFavorite = () => {
    if (isFavorite) {
      removeFromFavorites(product);
    } else {
      addToFavorites(product);
    }
    setIsFavorite(!isFavorite);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div className="product-card">
      <div className={`star-icon ${isFavorite ? 'favorite' : ''}`} onClick={handleToggleFavorite}></div>
      <h3>{product.name}</h3>
      <p>Price: {product.price}</p>
      <p>SKU: {product.sku}</p>
      <p>Color: {product.color}</p>
      <img className="product-img" src={product.image} alt={product.name} />
      <button className="add-button" onClick={handleAddToCart}>Add to cart</button>
      {isModalOpen && (
        <ModalText onClose={closeModal}>Product added to cart!</ModalText>
      )}
    </div>
  );
}

export default ProductCard;