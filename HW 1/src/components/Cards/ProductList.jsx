import React from 'react';
import ProductCard from './ProductCard';
import { useDisplayMode } from '../context/DisplayModeContext';


function ProductList({ products, addToCart, addToFavorites, removeFromFavorites }) {
  const { isGridView } = useDisplayMode();

  return (
    <div className={`product-list-container ${isGridView ? 'grid-view' : 'table-view'}`}>
      {isGridView ? (
        <div className="product-list">
          {products.map((product, index) => (
            <ProductCard 
              key={index} 
              product={product} 
              addToCart={addToCart} 
              addToFavorites={addToFavorites} 
              removeFromFavorites={removeFromFavorites} 
            />
          ))}
        </div>
      ) : (
        <table className="product-table">
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Price</th>
              <th>SKU</th>
              <th>Color</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {products.map((product, index) => (
              <tr key={index}>
                <td><img src={product.image} alt={product.name} className="img-table" /></td>
                <td>{product.name}</td>
                <td>{product.price}</td>
                <td>{product.sku}</td>
                <td>{product.color}</td>
                <td>
                  <button onClick={() => addToCart(product)}>Add to cart</button>
                  <button onClick={() => addToFavorites(product)}>Add to favorites</button>
                  {/* <button onClick={() => removeFromFavorites(product)}>Remove from favorites</button> */}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
}

export default ProductList;