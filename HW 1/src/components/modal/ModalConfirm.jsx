import React from 'react';
import ModalWrapper from './ModalWrapper';
import ModalClose from './ModalClose';
import ModalHeader from './ModalHeader';
import ModalBody from './ModalBody';
import ModalFooter from './ModalFooter';

const ModalConfirm = ({ onClose, onConfirm, children }) => {
  return (
    <ModalWrapper onClose={onClose}>
      <ModalClose onClick={onClose} />
      <ModalHeader>Confirm Action</ModalHeader>
      <ModalBody>{children}</ModalBody>
      <ModalFooter firstText="Yes, delete" firstClick={onConfirm} secondaryText="No, cancel" secondaryClick={onClose} />
    </ModalWrapper>
  );
};

export default ModalConfirm;