import React from 'react';
import ModalWrapper from './ModalWrapper.jsx'; 
import ModalHeader from './ModalHeader.jsx';
import ModalFooter from './ModalFooter.jsx';
import ModalClose from './ModalClose.jsx';
import ModalBody from './ModalBody.jsx';

const Modal = ({ onClose, children, buttonText }) => {
  return (
    <ModalWrapper>
      <ModalHeader>{children}</ModalHeader>
      <ModalClose onClick={onClose} />
      <ModalBody>{children}</ModalBody>
      <ModalFooter
        firstText={buttonText}
        firstClick={onClose}
      />
    </ModalWrapper>
  );
};

export default Modal;