// import './close.scss'
import React from 'react';

const ModalClose = ({ onClick }) => {
  return (
    <div className="modal-close" onClick={onClick}>
    </div>
  );
};

export default ModalClose;