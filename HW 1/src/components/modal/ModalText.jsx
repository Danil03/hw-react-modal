
import React from 'react';
import ModalWrapper from './ModalWrapper.jsx';
import ModalClose from './ModalClose.jsx'; 

import ModalBody from './ModalBody.jsx';

const ModalText = ({ onClose, children }) => {
  const handleConfirm = () => {
    onClose(); 
    
  };

  return (
    <ModalWrapper onClose={onClose}>
      <ModalClose onClick={onClose} /> 
     
      <ModalBody>
        {children}
        <p onClick={handleConfirm}></p> 
      </ModalBody>
    </ModalWrapper>
  );
};

export default ModalText;