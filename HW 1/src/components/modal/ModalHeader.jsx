// import './header.scss'
const ModalHeader = ({ children }) => {
    return (
      <button className="modal-header">
        {children}
      </button>
    );
  };
  export default ModalHeader;