const ModalFooter = ({ firstText, secondaryText, firstClick, secondaryClick }) => {
    return (
      <div className="modal-footer">
        {firstText && <button className="confirm-but" onClick={firstClick}>{firstText}</button>}
        {secondaryText && <button className="confirm-but"  onClick={secondaryClick}>{secondaryText}</button>}
      </div>
    );
  };
  
  export default ModalFooter;