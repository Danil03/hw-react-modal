// import './wrapper.scss';

const ModalWrapper = ({ onClose, children }) => {
  const handleBackgroundClick = () => {
    onClose(); // Закрыть модальное окно при клике на затемненную область
  };

  return (
    <div className="modal-wrapper" onClick={handleBackgroundClick}>
      <div className="modal-content">
        {children}
      </div>
    </div>
  );
};

export default ModalWrapper;